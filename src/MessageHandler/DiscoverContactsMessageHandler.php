<?php

namespace App\MessageHandler;

use App\Api\HunterApiClient;
use App\Entity\Domain;
use App\Entity\DomainContact;
use App\Entity\UploadList;
use App\Api\Exception\HuntApiException;
use App\Message\DiscoverContacts;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class DiscoverContactsMessageHandler implements MessageHandlerInterface
{
    /**
     * @var EntityRepository
     */
    private $uploadListRepository;

    /**
     * @var EntityRepository
     */
    private $domainContactRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var HunterApiClient
     */
    private $apiClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param HunterApiClient $apiClient
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(HunterApiClient $apiClient, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->uploadListRepository = $entityManager->getRepository(UploadList::class);
        $this->domainContactRepository = $entityManager->getRepository(DomainContact::class);
        $this->entityManager = $entityManager;
        $this->apiClient = $apiClient;
        $this->logger = $logger;
    }

    /**
     * @param DiscoverContacts $discoverContacts
     */
    public function __invoke(DiscoverContacts $discoverContacts)
    {
        /** @var UploadList $uploadList */
        $uploadList = $this->uploadListRepository->find($discoverContacts->getUploadListId());

        foreach ($uploadList->getDomains() as $domain) {
            try {
                $this->handleDomain($domain);

            } catch (HuntApiException $e) {
                $this->logger->error($e->getMessage());
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @param Domain $domain
     *
     * @throws \App\Api\Exception\HuntApiException
     */
    private function handleDomain(Domain $domain): void
    {
        $response = $this->apiClient->searchDomain($domain->getDomainName());

        foreach ($response->data->emails as $emailData) {
            $domainContact = $this->domainContactRepository->findOneBy(['email' => $emailData->email]);

            if (null === $domainContact) {
                $domainContact = new DomainContact();
                $domainContact->setEmail($emailData->email);
            }

            $domainContact->setFirstName($emailData->firstName);
            $domainContact->setLastName($emailData->lastName);
            $domainContact->setConfidence($emailData->confidence);
            $domainContact->setDomain($domain);

            $this->entityManager->persist($domainContact);
        }
    }
}
