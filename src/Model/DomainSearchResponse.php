<?php

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class DomainSearchResponse {

    /**
     * @var DomainData
     *
     * @Serializer\Type("App\Model\DomainData")
     */
    public $data;

    /**
     * @var array|Error[]
     *
     * @Serializer\Type("array<App\Model\Error>")
     */
    public $errors;
}