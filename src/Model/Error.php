<?php

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class Error
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    public $id;

    /**
     * @var integer
     *
     * @Serializer\Type("integer")
     */
    public $code;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    public $details;
}
