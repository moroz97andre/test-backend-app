<?php

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class DomainData
{
    /**
     * @var array|EmailData[]
     *
     * @Serializer\Type("array<App\Model\EmailData>")
     */
    public $emails;
}
