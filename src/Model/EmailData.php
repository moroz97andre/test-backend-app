<?php

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class EmailData
{
    /**
     * @var string
     *
     * @Serializer\SerializedName("value")
     * @Serializer\Type("string")
     */
    public $email;

    /**
     * @var string
     *
     * @Serializer\SerializedName("first_name")
     * @Serializer\Type("string")
     */
    public $firstName;

    /**
     * @var string
     *
     * @Serializer\SerializedName("last_name")
     * @Serializer\Type("string")
     */
    public $lastName;

    /**
     * @var integer
     *
     * @Serializer\Type("integer")
     */
    public $confidence;
}
