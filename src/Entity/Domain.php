<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"},
 *     normalizationContext={"groups"={"domain:output"}}
 * )
 * @ORM\Entity()
 * @UniqueEntity("domainName")
 */
class Domain
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"domain:output", "upload_list:input"})
     */
    private $id;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="UploadList", mappedBy="domains")
     */
    private $uploadLists;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="DomainContact", mappedBy="domain")
     * @Groups({"domain:output"})
     */
    private $domainContacts;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Groups({"domain:output", "upload_list:input"})
     */
    private $domainName;


    public function __construct()
    {
        $this->uploadLists = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * @param string $domainName
     */
    public function setDomainName($domainName)
    {
        $this->domainName = $domainName;
    }

    /**
     * @return Collection
     */
    public function getUploadLists(): Collection
    {
        return $this->uploadLists;
    }

    /**
     * @param Collection $uploadLists
     */
    public function setUploadLists(Collection $uploadLists)
    {
        $this->uploadLists = $uploadLists;
    }

    /**
     * @return Collection
     */
    public function getDomainContacts(): Collection
    {
        return $this->domainContacts;
    }

}
