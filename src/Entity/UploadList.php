<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Collection;

/**
 * @ApiResource(
 *     collectionOperations={"get", "post"},
 *     itemOperations={"get"},
 *     normalizationContext={"groups"={"upload_list:output"}},
 *     denormalizationContext={"groups"={"upload_list:input"}}
 * )
 * @ORM\Entity
 * @UniqueEntity("name")
 */
class UploadList
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"upload_list:output"})
     */
    private $id;

    /**
     * @var Collection|Domain[]
     *
     * @ORM\ManyToMany(targetEntity="Domain", inversedBy="uploadLists", cascade={"persist"})
     * @ORM\JoinTable(name="upload_domain")
     * @Groups({"upload_list:input"})
     * @ApiSubresource
     */
    private $domains;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Groups({"upload_list:output", "upload_list:input"})
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @Groups({"upload_list:output"})
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $uploadTime;

    public function __construct()
    {
        $this->domains = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return \DateTime
     */
    public function getUploadTime()
    {
        return $this->uploadTime;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param \DateTime $uploadTime
     */
    public function setUploadTime($uploadTime)
    {
        $this->uploadTime = $uploadTime;
    }

    /**
     * @return Collection|Domain[]
     */
    public function getDomains()
    {
        return $this->domains;
    }

    /**
     * @param Collection $domains
     */
    public function setDomains($domains)
    {
        $this->domains = $domains;
    }
}
