<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\UploadList;
use App\Repository\DomainRepository;
use Doctrine\ORM\EntityManagerInterface;

class UploadListDataPersister implements DataPersisterInterface
{
    /**
     * @var DomainRepository
     */
    private $domainRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(DomainRepository $domainRepository, EntityManagerInterface $entityManager)
    {
        $this->domainRepository = $domainRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function supports($data): bool
    {
        return $data instanceof UploadList;
    }

    /**
     * Persists the data.
     *
     * @return object|void Void will not be supported in API Platform 3, an object should always be returned
     */
    public function persist($data)
    {
        if ($data instanceof UploadList) {
            $domains = [];

            foreach ($data->getDomains() as $requestedDomain) {
                $url = $requestedDomain->getDomainName();
                $parsedUrl = parse_url($url);
                if (!isset($parsedUrl['host'])) {
                    continue;
                }

                $domainName = $parsedUrl['host'];

                $dbDomain = $this->domainRepository->findOneBy(
                    ['domainName' => $domainName]
                );

                $domain = $requestedDomain;
                if (null !== $dbDomain) {
                   $domain = $dbDomain;
                }

                $domain->setDomainName($domainName);
                $domains[] = $domain;
            }

            $data->setDomains($domains);

            $this->entityManager->persist($data);
            $this->entityManager->flush();
        }
    }

    /**
     * Removes the data.
     */
    public function remove($data)
    {

    }
}
