<?php

namespace App\Message;

class DiscoverContacts
{
    /**
     * @var integer
     */
    private $uploadListId;

    /**
     * @return integer
     */
    public function getUploadListId(): int
    {
        return $this->uploadListId;
    }

    /**
     * @param $uploadListId
     *
     * @return DiscoverContacts
     */
    public function setUploadListId($uploadListId): DiscoverContacts
    {
        $this->uploadListId = $uploadListId;

        return $this;
    }

}
