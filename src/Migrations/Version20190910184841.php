<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190910184841 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE domain (id INT AUTO_INCREMENT NOT NULL, domain_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE domain_contact (id INT AUTO_INCREMENT NOT NULL, domain_id INT DEFAULT NULL, first_name VARCHAR(45) NOT NULL, last_name VARCHAR(45) NOT NULL, email VARCHAR(255) NOT NULL, confidence SMALLINT NOT NULL, INDEX IDX_AEB5FF20115F0EE5 (domain_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE upload_list (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, upload_time DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE upload_domain (upload_list_id INT NOT NULL, domain_id INT NOT NULL, INDEX IDX_2A884EB3F8990C65 (upload_list_id), INDEX IDX_2A884EB3115F0EE5 (domain_id), PRIMARY KEY(upload_list_id, domain_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE domain_contact ADD CONSTRAINT FK_AEB5FF20115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id)');
        $this->addSql('ALTER TABLE upload_domain ADD CONSTRAINT FK_2A884EB3F8990C65 FOREIGN KEY (upload_list_id) REFERENCES upload_list (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE upload_domain ADD CONSTRAINT FK_2A884EB3115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE domain_contact DROP FOREIGN KEY FK_AEB5FF20115F0EE5');
        $this->addSql('ALTER TABLE upload_domain DROP FOREIGN KEY FK_2A884EB3115F0EE5');
        $this->addSql('ALTER TABLE upload_domain DROP FOREIGN KEY FK_2A884EB3F8990C65');
        $this->addSql('DROP TABLE domain');
        $this->addSql('DROP TABLE domain_contact');
        $this->addSql('DROP TABLE upload_list');
        $this->addSql('DROP TABLE upload_domain');
    }
}
