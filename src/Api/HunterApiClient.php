<?php

namespace App\Api;

use App\Api\Exception\HuntApiException;
use App\Model\DomainSearchResponse;
use Hiraya\Hunter;
use JMS\Serializer\SerializerInterface;

class HunterApiClient
{
    /**
     * @var Hunter
     */
    private $hunterClient;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer, string $apiKey)
    {
        $this->hunterClient = new Hunter($apiKey);
        $this->serializer = $serializer;
    }

    /**
     * @param string $domainName
     *
     * @return DomainSearchResponse
     */
    public function searchDomain(string $domainName): DomainSearchResponse
    {
        /** @var DomainSearchResponse $response */
        $response = $this->serializer->deserialize(
            $this->hunterClient->searchDomain($domainName),
            DomainSearchResponse::class,
            'json'
        );

        if (!empty($response->errors)) {
            $errorReasons = [];
            foreach ($response->errors as $error) {
                $errorReasons[] = $error->details;
            }

            throw new HuntApiException(
                sprintf(
                    'Hunter api error. Reasons: %s',
                    implode(',', $errorReasons)
                )
            );
        }

        return $response;
    }
}
