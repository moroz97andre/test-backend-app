<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\UploadList;
use App\Message\DiscoverContacts;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Messenger\MessageBusInterface;

class UploadListCreationSubscriber implements EventSubscriberInterface
{
    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @param MessageBusInterface $bus
     */
    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['discoverContacts', EventPriorities::POST_WRITE],
        ];
    }

    /**
     * @param ViewEvent $event
     */
    public function discoverContacts(ViewEvent $event): void
    {
        /** @var UploadList $uploadList */
        $uploadList = $event->getControllerResult();
        if (!($uploadList instanceof UploadList) || Request::METHOD_POST !== $event->getRequest()->getMethod()) {
            return;
        }

        $message = (new DiscoverContacts())->setUploadListId($uploadList->getId());
        $this->bus->dispatch($message);
    }
}
